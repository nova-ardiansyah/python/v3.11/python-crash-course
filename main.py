# SET

# food = {"apple", "banana", "cherry"}
# food.add("orange")
# food.update(["orange", "mango", "grapes"])
# food.clear()

# dinner = {"pizza", "pasta", "noodles", "chicken"}
# food.update(dinner)

# print(food)

# Dictionary

# capitals = {"USA": "Washington DC", "UK": "London", "India": "New Delhi"}
# capitals["USA"] = "New York"
# capitals["Germany"] = "Berlin"
# capitals.update({"France": "Paris", "Italy": "Rome"})

# print(capitals)

# Index Operator
# name = "nova ardiansyah"

# name = name.title()

# if (name[0].isupper()) :
#   print("Yes")
# else :
#   print("No")

# Function

# def hello(name) :
#   print("Hello World")
#   print("Have a nice day " + name)

# hello("Nova Ardiansyah")

# def multiply(num1, num2) :
#   return num1 * num2

# print(multiply(2, 4))

# Keyword Argument
# def hello(first, middle, last) :
#   print("Hello " + first + " " + middle + " " + last)

# hello(last = "Ardiansyah", first = "Nova", middle = "A")

# Nested function
# print(abs(float(input("Enter your money: "))))

# Variable Scope

# def display_name(name) :
#   last_name = "Ardiansyah"
#   print(name + " " + last_name)

# display_name("Nova")

# all arguments

# def add(*args) :
#   total = 0
#   for a in args :
#     total += a
#   print(total)

# add(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

# str format

animal = "dog"
action = "bite"

print("Does your {} {}?".format(animal, action))